import org.eclipse.jgit.lib.*;
import org.eclipse.jgit.transport.FetchResult;
import org.eclipse.jgit.transport.RefSpec;
import org.eclipse.jgit.transport.TrackingRefUpdate;
import org.eclipse.jgit.transport.Transport;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.eclipse.jgit.lib.Constants.R_HEADS;
import static org.eclipse.jgit.lib.Constants.R_REMOTES;
import static org.eclipse.jgit.lib.Constants.R_TAGS;

/**
 * @author Ivan Sungurov
 */
public class RepoTest {
    private static String longTypeOf(final TrackingRefUpdate u) {
        final RefUpdate.Result r = u.getResult();
        if (r == RefUpdate.Result.LOCK_FAILURE)
            return "[lock fail]";

        if (r == RefUpdate.Result.IO_FAILURE)
            return "[i/o error]";

        if (r == RefUpdate.Result.NEW) {
            if (u.getRemoteName().startsWith(Constants.R_HEADS))
                return "[new branch]";
            else if (u.getLocalName().startsWith(Constants.R_TAGS))
                return "[new tag]";
            return "[new]";
        }

        if (r == RefUpdate.Result.FORCED) {
            final String aOld = u.getOldObjectId().abbreviate(6).toString();
            final String aNew = u.getNewObjectId().abbreviate(6).toString();
            return aOld + "..." + aNew;
        }

        if (r == RefUpdate.Result.FAST_FORWARD) {
            final String aOld = u.getOldObjectId().abbreviate(6).toString();
            final String aNew = u.getNewObjectId().abbreviate(6).toString();
            return aOld + ".." + aNew;
        }

        if (r == RefUpdate.Result.REJECTED)
            return "[rejected]";
        if (r == RefUpdate.Result.NO_CHANGE)
            return "[up to date]";
        return "[" + r.name() + "]";
    }

    private static char shortTypeOf(final RefUpdate.Result r) {
        if (r == RefUpdate.Result.LOCK_FAILURE)
            return '!';
        if (r == RefUpdate.Result.IO_FAILURE)
            return '!';
        if (r == RefUpdate.Result.NEW)
            return '*';
        if (r == RefUpdate.Result.FORCED)
            return '+';
        if (r == RefUpdate.Result.FAST_FORWARD)
            return ' ';
        if (r == RefUpdate.Result.REJECTED)
            return '!';
        if (r == RefUpdate.Result.NO_CHANGE)
            return '=';
        return ' ';
    }

    protected static String abbreviateRef(String dst, boolean abbreviateRemote) {
        if (dst.startsWith(R_HEADS))
            return dst.substring(R_HEADS.length());
        else if (dst.startsWith(R_TAGS))
            return dst.substring(R_TAGS.length());
        else if (abbreviateRemote && dst.startsWith(R_REMOTES))
            return dst.substring(R_REMOTES.length());
        return dst;
    }

    public static void main(String[] args) throws Exception {
        String path = "D:/work/jiragit5";
        File root = new File(path);
        RepositoryBuilder builder = new RepositoryBuilder().addCeilingDirectory(root).findGitDir(root);
        if(builder.getGitDir() == null) {
            builder.setGitDir(root);
        }

        Repository repository = builder.build();
        System.out.println(repository.getObjectDatabase().exists());
        repository.scanForRepoChanges();

        // git+ssh://play@git.yotalab.ru/mci
        Transport tn = Transport.open(repository, "origin");
        final FetchResult r;
        List<RefSpec> toget = new ArrayList<RefSpec>();
        toget.add(new RefSpec("refs/heads/*:refs/heads/*"));
        try {
            r = tn.fetch(new TextProgressMonitor(), toget);

            if (r.getTrackingRefUpdates().isEmpty()) {
                System.out.println("No updates");
                return;
            }
        } finally {
            tn.close();
        }

        boolean shownURI = false;
        for (final TrackingRefUpdate u : r.getTrackingRefUpdates()) {

            final char type = shortTypeOf(u.getResult());
            final String longType = longTypeOf(u);
            final String src = abbreviateRef(u.getRemoteName(), false);
            final String dst = abbreviateRef(u.getLocalName(), true);

            if (!shownURI) {
                shownURI = true;
            }

            System.out.println(String.format(" %c %-17s %-10s -> %s", type, longType, src, dst));
        }

        for(Map.Entry<String, Ref> pair : repository.getAllRefs().entrySet()) {
            System.out.println(pair.getKey());
        }

        System.out.println(repository.getRef("HEAD"));
    }
}
